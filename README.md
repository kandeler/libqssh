# libqssh

An SSH client library with a Qt API.

## Prerequisites

* Qt >= 5.9
* The Botan crypto library, version 2.x

Any platform that provides these should be supported.

## Building instructions

On a Linux system with all dependencies installed, things are straightforward:
```bash
$ mkdir libqssh_build_dir && cd libqssh_build_dir
$ qmake qssh_source_dir && make # or qbs -f qssh_source_dir
```
If the Botan library cannot be found via `pkg-config` (e.g. on Windows), then you need to provide
its location:
```bash
$ qmake QSSH_BOTAN_INCLUDE_DIR=<botan headers dir> QSSH_BOTAN_LIBRARY_DIR=<botan library dir>
```

## How to use the API

The following code snippet demonstrates how to establish a connection, run a remote process
and upload a file. For complete applications, take a look at the `src/examples` directory
in the repository.

```cpp
using namespace QSsh;
SshConnectionParameters params;
params.setHost("myhost");
params.setUserName("myuser");
params.privateKeyFile = QDir::homePath() + "/.ssh/id_rsa";
SshConnection * const connection = new SshConnection(params); // from <qssh/sshconnection.h>
QObject::connect(connection, &SshConnection::error, [connection] {
    std::cerr << "Connection error: " << qPrintable(connection->errorString()) << std::endl;
});
QObject::connect(connection, &SshConnection::connected, [connection] {
    std::cout << "connection established" << std::endl;
    const SshRemoteProcess::Ptr proc = connection->createRemoteProcess("ls"); // from <qssh/sshremoteprocess.h>
    QObject::connect(proc.data(), &SshRemoteProcess::readyReadStandardOutput, [p = proc.data()] {
        std::cout << p->readAllStandardOutput().constData() << std::flush;
    });
    QObject::connect(proc.data(), &SshRemoteProcess::readyReadStandardError, [p = proc.data()] {
        std::cerr << p->readAllStandardError().constData() << std::flush;
    });
    QObject::connect(proc.data(), &SshRemoteProcess::closed, [p = proc.data()] {
        std::cout << "Process finished with exit code " << p->exitCode() << std::endl;
    });
    std::cout << "Starting remote process..." << std::endl;
    proc->start();
    const SftpChannel::Ptr sftpChannel = connection->createSftpChannel(); // from <qssh/sftpchannel.h>
    QObject::connect(sftpChannel.data(), &SftpChannel::initialized, [sftp = sftpChannel.data()] {
        std::cout << "SFTP session set up." << std::endl;
        QObject::connect(sftp, &SftpChannel::finished, [](SftpJobId, const QString &error) {
            if (!error.isEmpty())
                std::cerr << "SFTP operation failed: " << qPrintable(error) << std::endl;
            else
                std::cout << "SFTP operation finished successfully." << std::endl;
        });
        std::cout << "Uploading file..." << std::endl;
        sftp->uploadFile("/tmp/localfile.txt", "/tmp/remotefile.txt", SftpOverwriteExisting);
    });
    std::cout << "Initializing SFTP channel..." << std::endl;
    sftpChannel->initialize();
});
std::cout << "Setting up connection ..." << std::endl;
connection->connectToHost();
```
