import qbs
import qbs.FileInfo

QtApplication {
    Depends { name: "qssh" }
    cpp.cxxLanguageVersion: "c++17"
    cpp.defines: project.commonDefines
    cpp.rpaths: FileInfo.joinPaths(qbs.installRoot, qbs.installPrefix, project.libInstallDir)
}
