import qbs

Project {
    references: ["examples", "lib"].concat(project.developerBuild ? "test" : []);
}
