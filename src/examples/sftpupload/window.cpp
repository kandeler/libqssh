#include "window.h"

#include <ui_window.h>

#include <qssh/sftpchannel.h>
#include <qssh/sshconnection.h>
#include <qssh/sshconnectionmanager.h>

#include <QFileDialog>

using namespace QSsh;

Window::Window() : QDialog(), m_ui(new MyUi::Ui::Window)
{
    m_ui->setupUi(this);
    for (QLineEdit * const lineEdit
         : QList<QLineEdit *>{m_ui->hostLineEdit, m_ui->userLineEdit,
                              m_ui->localFileLineEdit, m_ui->remoteFileLineEdit}) {
        connect(lineEdit, &QLineEdit::textChanged, this, &Window::setButtonState);
    }
    connect(m_ui->connectButton, &QAbstractButton::clicked, this, [this] {
        if (!m_connection || m_connection->state() == SshConnection::Unconnected)
            connectToHost();
        else
            m_connection->disconnectFromHost();
    });
    connect(m_ui->chooseLocalFileButton, &QPushButton::clicked, this, &Window::chooseLocalFile);
    connect(m_ui->uploadButton, &QAbstractButton::clicked, this, &Window::uploadFile);
    setButtonState();
}

void Window::setButtonState()
{
    if (!m_connection || m_connection->state() == SshConnection::Unconnected) {
        m_ui->connectButton->setText(tr("Connect"));
        m_ui->connectButton->setEnabled(!host().isEmpty() && !user().isEmpty());
    } else if (m_connection->state() == SshConnection::Connecting) {
        m_ui->connectButton->setText(tr("Cancel"));
    } else {
        m_ui->connectButton->setText(tr("Disconnect"));
    }
    m_ui->uploadButton->setEnabled(m_sftpChannel
                                   && m_sftpChannel->state() == SftpChannel::Initialized
                                   && !localFile().isEmpty() && !remoteFile().isEmpty());
}

QString Window::host() const
{
    return m_ui->hostLineEdit->text();
}

QString Window::user() const
{
    return m_ui->userLineEdit->text();
}

QString Window::localFile() const
{
    return m_ui->localFileLineEdit->text();
}

QString Window::remoteFile() const
{
    return m_ui->remoteFileLineEdit->text();
}

Window::~Window()
{
    if (m_sftpChannel)
        m_sftpChannel->disconnect();
    if (m_connection) {
        disconnect(m_connection, nullptr, this, nullptr);
        releaseConnection(m_connection);
    }
    delete m_ui;
}

void Window::connectToHost()
{
    SshConnectionParameters sshParams;
    sshParams.setHost(host());
    sshParams.setUserName(user());
    sshParams.authenticationType
            = SshConnectionParameters::AuthenticationTypeTryAllPasswordBasedMethods;
    sshParams.setPassword(m_ui->passwordLineEdit->text());
    sshParams.setPort(m_ui->portSpinBox->value());
    sshParams.timeout = 10;
    m_connection = acquireConnection(sshParams);
    connect(m_connection, &SshConnection::connected, this, [this] {
        printInfo(tr("Connection established."));
        setButtonState();
        setupSftp();
    });
    connect(m_connection, &SshConnection::disconnected, this, [this] {
        if (!m_connection)
            return;
        printInfo(tr("Disconnected."));
        handleConnectionFinished();
    });
    connect(m_connection, &SshConnection::error, this, [this] {
        printError(tr("Connection error: %1").arg(m_connection->errorString()));
        handleConnectionFinished();
    });
    setButtonState();
    switch (m_connection->state()) {
    case SshConnection::Connected:
        setupSftp();
        break;
    case SshConnection::Unconnected:
        printInfo(tr("Connecting..."));
        m_connection->connectToHost();
        break;
    case SshConnection::Connecting:
        break;
    }
}

void Window::setupSftp()
{
    printInfo(tr("Establishing SFTP session..."));
    m_sftpChannel = m_connection->createSftpChannel();
    connect(m_sftpChannel.data(), &SftpChannel::initialized, [this] {
        printInfo(tr("SFTP session established."));
        setButtonState();
    });
    connect(m_sftpChannel.data(), &SftpChannel::channelError, [this](const QString &error) {
        printError(tr("SFTP error: %1").arg(error));
        setButtonState();
    });
    connect(m_sftpChannel.data(), &SftpChannel::closed, [this] {
        if (!m_sftpChannel)
            return;
        printInfo(tr("SFTP channel closed."));
        setButtonState();
    });
    connect(m_sftpChannel.data(), &SftpChannel::finished,
            [this](SftpJobId id, const QString &error) {
        const bool idKnown = m_activeJobs.removeOne(id);
        if (!idKnown) {
            printError(tr("Received status about unknown upload job id %1").arg(id));
            return;
        }
        if (!error.isEmpty()) {
            printError(tr("Upload job %1 failed: %2").arg(id).arg(error));
            return;
        }
        printInfo(tr("Upload job %1 finished successfully.").arg(id));
    });
    m_sftpChannel->initialize();
}

void Window::uploadFile()
{
    const SftpJobId id = m_sftpChannel->uploadFile(localFile(), remoteFile(), SftpSkipExisting);
    if (id == SftpInvalidJob) {
        printError(tr("Upload failure: Could not read local file."));
        return;
    }
    printInfo(tr("Upload job %1 started for local file %2").arg(id).arg(localFile()));
    m_activeJobs << id;
}

void Window::chooseLocalFile()
{
    const QString file = QFileDialog::getOpenFileName(this, tr("Choose file to upload"));
    if (!file.isEmpty()) {
        m_ui->localFileLineEdit->setText(file);
        setButtonState();
    }
}

void Window::handleSftpOperationFinished(SftpJobId jobId, const QString &error)
{
    QString message;
    if (error.isEmpty())
        message = tr("Operation %1 finished successfully.").arg(jobId);
    else
        message = tr("Operation %1 failed: %2.").arg(jobId).arg(error);
    m_ui->outputTextEdit->appendPlainText(message);
}

void Window::printInfo(const QString &message)
{
    printText(message, "blue");
}

void Window::printError(const QString &message)
{
    printText(message, "red");
}

void Window::printText(const QString &text, const QString &color)
{
    const QString htmlText = QString::fromLatin1("<b><font color=\"%1\">%2</font></b>")
            .arg(color, text.toHtmlEscaped());
    m_ui->outputTextEdit->appendHtml(htmlText);
}

void Window::handleConnectionFinished()
{
    disconnect(m_connection, nullptr, this, nullptr);
    releaseConnection(m_connection);
    m_connection = nullptr;
    if (m_sftpChannel) {
        m_sftpChannel->disconnect();
        m_sftpChannel->deleteLater();
        m_sftpChannel = nullptr;
    }
    setButtonState();
}
