#include <qssh/sftpdefs.h>

#include <QDialog>
#include <QList>
#include <QSharedPointer>

namespace MyUi {namespace Ui { class Window; } }
namespace QSsh {
class SftpChannel;
class SshConnection;
}

class Window : public QDialog
{
    Q_OBJECT
public:
    Window();
    ~Window();

private:
    void setButtonState();
    void connectToHost();
    void setupSftp();
    void uploadFile();
    void chooseLocalFile();
    void handleSftpOperationFinished(QSsh::SftpJobId jobId, const QString &error);
    QString host() const;
    QString user() const;
    QString localFile() const;
    QString remoteFile() const;
    void printInfo(const QString &message);
    void printError(const QString &message);
    void printText(const QString &text, const QString &color);
    void handleConnectionFinished();

    QList<QSsh::SftpJobId> m_activeJobs;
    QSsh::SshConnection *m_connection = nullptr;
    QSharedPointer<QSsh::SftpChannel> m_sftpChannel;
    MyUi::Ui::Window * const m_ui;
};
