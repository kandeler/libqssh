#include "window.h"

#include <qssh/sftpfilesystemmodel.h>
#include <qssh/sshconnection.h>

#include <QApplication>
#include <QTreeView>

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    SftpFsWindow w;
    w.show();
    return app.exec();
}
