#include <qssh/sftpdefs.h>

#include <QDialog>

QT_BEGIN_NAMESPACE
namespace Ui { class Window; }
QT_END_NAMESPACE

namespace QSsh { class SftpFileSystemModel; }

class SftpFsWindow : public QDialog
{
    Q_OBJECT
public:
    SftpFsWindow(QWidget *parent = nullptr);
    ~SftpFsWindow();

private:
    void connectToHost();
    void downloadFile();
    void handleConnectionError(const QString &errorMessage);
    void handleSftpOperationFailed(const QString &errorMessage);
    void handleSftpOperationFinished(QSsh::SftpJobId jobId, const QString &error);

    QSsh::SftpFileSystemModel *m_fsModel;
    Ui::Window *m_ui;
};
