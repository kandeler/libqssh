#include "window.h"

#include <ui_window.h>

#include <qssh/sshremoteprocessrunner.h>

#include <QByteArray>
#include <QString>

using namespace QSsh;

Window::Window() : QDialog(), m_ui(new MyUi::Ui::Window)
{
    m_ui->setupUi(this);
    for (QLineEdit * const lineEdit
         : QList<QLineEdit *>{m_ui->hostLineEdit, m_ui->userLineEdit, m_ui->commandLineEdit}) {
        connect(lineEdit, &QLineEdit::textChanged, this, &Window::setRunButtonState);
    }
    connect(m_ui->runButton, &QPushButton::clicked, [this] {
        if (m_command)
            stopCommand();
        else
            runCommand();
    });
    setRunButtonState();
}

void Window::setRunButtonState()
{
    if (m_command) {
        m_ui->runButton->setText(tr("Stop"));
        m_ui->runButton->setEnabled(true);
    } else {
        m_ui->runButton->setText(tr("Run"));
        const bool validInput = !host().isEmpty() && !user().isEmpty() && !command().isEmpty();
        m_ui->runButton->setEnabled(validInput);
    }
}

void Window::printInfo(const QString &message)
{
    printText(message, "blue", true);
}

void Window::printError(const QString &message)
{
    printText(message, "red", true);
}

void Window::printRemoteStdout()
{
    printText(QString::fromUtf8(m_command->readAllStandardOutput()), "black", false);
}

void Window::printRemoteStderr()
{
    printText(QString::fromUtf8(m_command->readAllStandardError()), "red", false);
}

void Window::printText(const QString &text, const QString &color, bool bold)
{
    QString htmlText = QString::fromLatin1("<font color=\"%1\">%2</font>")
            .arg(color, text.toHtmlEscaped());
    if (bold)
        htmlText.prepend("<b>").append("</b>");
    m_ui->outputTextEdit->appendHtml(htmlText);
}

void Window::handleCommandFinished()
{
    m_command->disconnect();
    m_command->deleteLater();
    m_command = nullptr;
    setRunButtonState();
}

void Window::runCommand()
{
    printInfo(tr("Starting process..."));
    m_command = new SshRemoteProcessRunner(this);
    connect(m_command, &SshRemoteProcessRunner::connectionError, [this] {
        printError(tr("Connection error: %1").arg(m_command->lastConnectionErrorString()));
        handleCommandFinished();
    });
    connect(m_command, &SshRemoteProcessRunner::processStarted, [this] {
        printInfo("Remote process started.");
    });
    connect(m_command, &SshRemoteProcessRunner::processClosed, [this] {
        switch (m_command->processExitStatus()) {
        case SshRemoteProcess::FailedToStart:
        case SshRemoteProcess::CrashExit:
            printError(m_command->processErrorString());
            break;
        case SshRemoteProcess::NormalExit: {
            if (m_command->processExitCode() == 0) {
                printInfo(tr("Remote process finished normally."));
            } else {
                printError(tr("Remote process finished with exit code %1.")
                           .arg(m_command->processExitCode()));
            }
        }
        }
        handleCommandFinished();
    });
    connect(m_command, &SshRemoteProcessRunner::readyReadStandardOutput,
            this, &Window::printRemoteStdout);
    connect(m_command, &SshRemoteProcessRunner::readyReadStandardError,
            this, &Window::printRemoteStderr);
    SshConnectionParameters params;
    params.setHost(host());
    params.setUserName(user());
    params.setPassword(password());
    params.setPort(m_ui->portSpinBox->value());
    params.authenticationType
            = SshConnectionParameters::AuthenticationTypeTryAllPasswordBasedMethods;
    params.timeout = 10;
    m_command->run(m_ui->commandLineEdit->text().toUtf8(), params);
    setRunButtonState();
}

void Window::stopCommand()
{
    printInfo(tr("Sending kill signal. Note that this will not work with an OpenSSH "
                 "server < 7.9."));
    m_command->sendSignalToProcess(SshRemoteProcess::KillSignal);
}

QString Window::host() const
{
    return m_ui->hostLineEdit->text();
}

QString Window::user() const
{
    return m_ui->userLineEdit->text();
}

QString Window::password() const
{
    return m_ui->passwordLineEdit->text();
}

QString Window::command() const
{
    return m_ui->commandLineEdit->text();
}
