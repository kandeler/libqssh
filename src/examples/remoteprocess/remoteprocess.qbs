import qbs

QSshTestApp {
    Depends { name: "Qt.widgets" }
    files: [
        "main.cpp",
        "window.cpp",
        "window.h",
        "window.ui",
    ]
}
