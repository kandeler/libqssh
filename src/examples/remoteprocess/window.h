#pragma once

#include <QDialog>

namespace MyUi { namespace Ui { class Window; } }
namespace QSsh { class SshRemoteProcessRunner; }

class Window : public QDialog
{
public:
    Window();

private:
    void setRunButtonState();
    void printInfo(const QString &message);
    void printError(const QString &message);
    void printRemoteStdout();
    void printRemoteStderr();
    void printText(const QString &text, const QString &color, bool bold);
    void handleCommandFinished();
    void runCommand();
    void stopCommand();
    QString host() const;
    QString user() const;
    QString password() const;
    QString command() const;

    MyUi::Ui::Window * const m_ui;
    QSsh::SshRemoteProcessRunner *m_command = nullptr;
};
