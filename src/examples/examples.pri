include(../common.pri)

TEMPLATE = app
QT = core network
CONFIG   += console
CONFIG   -= app_bundle
INCLUDEPATH += $$QSSH_INCLUDE_DIR
LIBS += -L$$QSSH_LIB_DIR -lqssh
DESTDIR = $$QSSH_BIN_DIR
