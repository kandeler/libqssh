#include "shell.h"

#include <qssh/sshconnection.h>
#include <qssh/sshremoteprocess.h>

#include <QCoreApplication>
#include <QFile>
#include <QSocketNotifier>

#include <cstdlib>
#include <iostream>

using namespace QSsh;

Shell::Shell(const SshConnectionParameters &parameters, QObject *parent)
    : QObject(parent),
      m_connection(new SshConnection(parameters)),
      m_stdin(new QFile(this))
{
    connect(m_connection, &SshConnection::connected, this, &Shell::handleConnected);
    connect(m_connection, &SshConnection::dataAvailable, this, &Shell::handleShellMessage);
    connect(m_connection, &SshConnection::error, this, &Shell::handleConnectionError);
}

Shell::~Shell()
{
    delete m_connection;
}

void Shell::run()
{
    if (!m_stdin->open(stdin, QIODevice::ReadOnly | QIODevice::Unbuffered)) {
        std::cerr << "Error: Cannot read from standard input." << std::endl;
        QCoreApplication::exit(EXIT_FAILURE);
        return;
    }

    m_connection->connectToHost();
}

void Shell::handleConnectionError()
{
    std::cerr << "SSH connection error: " << qPrintable(m_connection->errorString()) << std::endl;
    QCoreApplication::exit(EXIT_FAILURE);
}

void Shell::handleShellMessage(const QString &message)
{
    std::cout << qPrintable(message);
}

void Shell::handleConnected()
{
    m_shell = m_connection->createRemoteShell();
    connect(m_shell.data(), &SshRemoteProcess::started, this, &Shell::handleShellStarted);
    connect(m_shell.data(), &SshRemoteProcess::readyReadStandardOutput,
            this, &Shell::handleRemoteStdout);
    connect(m_shell.data(), &SshRemoteProcess::readyReadStandardError,
            this, &Shell::handleRemoteStderr);
    connect(m_shell.data(), &SshRemoteProcess::closed, this, &Shell::handleChannelClosed);
    m_shell->start();
}

void Shell::handleShellStarted()
{
    QSocketNotifier * const notifier = new QSocketNotifier(0, QSocketNotifier::Read, this);
    connect(notifier, &QSocketNotifier::activated, this, &Shell::handleStdin);
}

void Shell::handleRemoteStdout()
{
    std::cout << m_shell->readAllStandardOutput().data() << std::flush;
}

void Shell::handleRemoteStderr()
{
    std::cerr << m_shell->readAllStandardError().data() << std::flush;
}

void Shell::handleChannelClosed(int exitStatus)
{
    std::cerr << "Shell closed. Exit status was " << exitStatus << ", exit code was "
        << m_shell->exitCode() << "." << std::endl;
    QCoreApplication::exit(exitStatus == SshRemoteProcess::NormalExit && m_shell->exitCode() == 0
        ? EXIT_SUCCESS : EXIT_FAILURE);
}

void Shell::handleStdin()
{
    m_shell->write(m_stdin->readLine());
}
