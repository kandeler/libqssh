#include "sshlogging_p.h"

namespace QSsh {
namespace Internal {
Q_LOGGING_CATEGORY(sshLog, "qssh", QtWarningMsg)
} // namespace Internal
} // namespace QSsh
