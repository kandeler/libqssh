#pragma once
#define SSHERRORS_P_H

#include <QMetaType>

namespace QSsh {

enum SshError {
    SshNoError, SshSocketError, SshTimeoutError, SshProtocolError,
    SshHostKeyError, SshKeyFileError, SshAuthenticationError,
    SshClosedByServerError, SshAgentError, SshInternalError
};

} // namespace QSsh

Q_DECLARE_METATYPE(QSsh::SshError)
