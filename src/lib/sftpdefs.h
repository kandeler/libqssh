#pragma once

#include "ssh_global.h"

#include <QFile>
#include <QString>

namespace QSsh {

typedef quint32 SftpJobId;
QSSH_EXPORT extern const SftpJobId SftpInvalidJob;

enum SftpOverwriteMode {
    SftpOverwriteExisting, SftpAppendToExisting, SftpSkipExisting
};

enum SftpFileType { FileTypeRegular, FileTypeDirectory, FileTypeOther, FileTypeUnknown };

class QSSH_EXPORT SftpFileInfo
{
public:
    QString name;
    SftpFileType type = FileTypeUnknown;
    quint64 size = 0;
    QFile::Permissions permissions;
    quint32 mtime = 0;

    // The RFC allows an SFTP server not to support any file attributes beyond the name.
    bool sizeValid = false;
    bool permissionsValid = false;
    bool mtimeValid = false;
};

} // namespace QSsh
