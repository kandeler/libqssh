#pragma once

#include "ssh_global.h"

#include <QString>
#include <QByteArray>

namespace QSsh {
namespace Internal {

class QSSH_AUTOTEST_EXPORT X11DisplayInfo
{
public:
    QString displayName;
    QString hostName;
    QByteArray protocol;
    QByteArray cookie;
    QByteArray randomCookie;
    int display = 0;
    int screen = 0;
};

} // namespace Internal
} // namespace QSsh
