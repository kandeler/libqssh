#pragma once

#include <QLoggingCategory>

namespace QSsh {
namespace Internal {
Q_DECLARE_LOGGING_CATEGORY(sshLog)
} // namespace Internal
} // namespace QSsh
