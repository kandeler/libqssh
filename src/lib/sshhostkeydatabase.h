#pragma once

#include "ssh_global.h"

#include <QSharedPointer>

QT_BEGIN_NAMESPACE
class QByteArray;
class QString;
QT_END_NAMESPACE

namespace QSsh {
class SshHostKeyDatabase;
typedef QSharedPointer<SshHostKeyDatabase> SshHostKeyDatabasePtr;

class QSSH_EXPORT SshHostKeyDatabase
{
    friend class QSharedPointer<SshHostKeyDatabase>; // To give create() access to our constructor.

public:
    enum KeyLookupResult {
        KeyLookupMatch,
        KeyLookupNoMatch,
        KeyLookupMismatch
    };

    ~SshHostKeyDatabase();

    bool load(const QString &filePath, QString *error = 0);
    bool store(const QString &filePath, QString *error = 0) const;
    KeyLookupResult matchHostKey(const QString &hostName, const QByteArray &key) const;
    void insertHostKey(const QString &hostName, const QByteArray &key);

private:
    SshHostKeyDatabase();

    class SshHostKeyDatabasePrivate;
    SshHostKeyDatabasePrivate * const d;
};

} // namespace QSsh
