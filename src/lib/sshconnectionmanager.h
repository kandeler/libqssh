#pragma once

#include "ssh_global.h"

namespace QSsh {

class SshConnection;
class SshConnectionParameters;

QSSH_EXPORT SshConnection *acquireConnection(const SshConnectionParameters &sshParams);
QSSH_EXPORT void releaseConnection(SshConnection *connection);

// Make sure the next acquireConnection with the given parameters will return a new connection.
QSSH_EXPORT void forceNewConnection(const SshConnectionParameters &sshParams);

} // namespace QSsh
