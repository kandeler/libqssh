#pragma once

#include "sshtcpiptunnel_p.h"

namespace QSsh {
class SshDirectTcpIpTunnel;

namespace Internal {

class SshDirectTcpIpTunnelPrivate : public SshTcpIpTunnelPrivate
{
    Q_OBJECT

    friend class QSsh::SshDirectTcpIpTunnel;

public:
    explicit SshDirectTcpIpTunnelPrivate(quint32 channelId, const QString &originatingHost,
            quint16 originatingPort, const QString &remoteHost, quint16 remotePort,
            SshSendFacility &sendFacility);

signals:
    void initialized();

private:
    void handleOpenSuccessInternal();

    const QString m_originatingHost;
    const quint16 m_originatingPort;
    const QString m_remoteHost;
    const quint16 m_remotePort;
};

} // namespace Internal
} // namespace QSsh
