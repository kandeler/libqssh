#include "sshkeypasswordretriever_p.h"

#include <QString>
#include <QApplication>
#include <QInputDialog>

#include <iostream>

namespace QSsh {
namespace Internal {

std::string get_passphrase()
{
    const bool hasGui = dynamic_cast<QApplication *>(QApplication::instance());
    if (hasGui) {
        const QString &password = QInputDialog::getText(0,
            QCoreApplication::translate("QSsh::Ssh", "Password Required"),
            QCoreApplication::translate("QSsh::Ssh", "Please enter the password for your private key."),
            QLineEdit::Password, QString());
        return std::string(password.toLocal8Bit().data());
    } else {
        std::string password;
        std::cout << "Please enter the password for your private key (set echo off beforehand!): " << std::flush;
        std::cin >> password;
        return password;
    }
}

} // namespace Internal
} // namespace QSsh
