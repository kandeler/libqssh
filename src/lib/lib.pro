include(../common.pri)
include(lib.pri)

TEMPLATE = lib
TARGET = qssh
QT = core network widgets
DEFINES += QSSH_LIBRARY
DESTDIR = $$QSSH_LIB_DIR
DLLDESTDIR = $$QSSH_BIN_DIR

SOURCES = \
    opensshkeyfilereader.cpp \
    sftpchannel.cpp \
    sftpdefs.cpp \
    sftpfilesystemmodel.cpp \
    sftpincomingpacket.cpp \
    sftpoperation.cpp \
    sftpoutgoingpacket.cpp \
    sftppacket.cpp \
    sshagent.cpp \
    sshcapabilities.cpp \
    sshchannel.cpp \
    sshchannelmanager.cpp \
    sshconnection.cpp \
    sshconnectionmanager.cpp \
    sshcryptofacility.cpp \
    sshdirecttcpiptunnel.cpp \
    sshforwardedtcpiptunnel.cpp \
    sshhostkeydatabase.cpp \
    sshincomingpacket.cpp \
    sshkeycreationdialog.cpp \
    sshkeyexchange.cpp \
    sshkeygenerator.cpp \
    sshkeypasswordretriever.cpp \
    sshlogging.cpp \
    sshoutgoingpacket.cpp \
    sshpacket.cpp \
    sshpacketparser.cpp \
    sshremoteprocess.cpp \
    sshremoteprocessrunner.cpp \
    sshsendfacility.cpp \
    sshtcpipforwardserver.cpp \
    sshtcpiptunnel.cpp \
    sshx11channel.cpp \
    sshx11inforetriever.cpp

HEADERS = \
    opensshkeyfilereader_p.h \
    sftpchannel.h \
    sftpchannel_p.h \
    sftpdefs.h \
    sftpfilesystemmodel.h \
    sftpincomingpacket_p.h \
    sftpoperation_p.h \
    sftpoutgoingpacket_p.h \
    sftppacket_p.h \
    ssh_global.h \
    sshagent_p.h \
    sshbotanconversions_p.h \
    sshcapabilities_p.h \
    sshchannel_p.h \
    sshchannelmanager_p.h \
    sshconnection.h \
    sshconnection_p.h \
    sshconnectionmanager.h \
    sshcryptofacility_p.h \
    sshdirecttcpiptunnel.h \
    sshdirecttcpiptunnel_p.h \
    ssherrors.h \
    sshexception_p.h \
    sshforwardedtcpiptunnel.h \
    sshforwardedtcpiptunnel_p.h \
    sshhostkeydatabase.h \
    sshincomingpacket_p.h \
    sshkeycreationdialog.h \
    sshkeyexchange_p.h \
    sshkeygenerator.h \
    sshkeypasswordretriever_p.h \
    sshlogging_p.h \
    sshoutgoingpacket_p.h \
    sshpacket_p.h \
    sshpacketparser_p.h \
    sshpseudoterminal.h \
    sshremoteprocess.h \
    sshremoteprocess_p.h \
    sshremoteprocessrunner.h \
    sshsendfacility_p.h \
    sshtcpipforwardserver.h \
    sshtcpipforwardserver_p.h \
    sshtcpiptunnel_p.h \
    sshx11channel_p.h \
    sshx11displayinfo_p.h \
    sshx11inforetriever_p.h

FORMS = sshkeycreationdialog.ui

RESOURCES += ssh.qrc

api_headers.files = $$API_HEADERS
api_headers.path = $$QSSH_INSTALL_PREFIX/include/qssh
target.path = $$QSSH_INSTALL_PREFIX/$$QSSH_LIBDIRNAME
INSTALLS = api_headers target

isEmpty(QSSH_BOTAN_INCLUDE_DIR) {
    CONFIG += link_pkgconfig
    PKGCONFIG += botan-2
} else {
    INCLUDEPATH += $$QSSH_BOTAN_INCLUDE_DIR
    !isEmpty(QSSH_BOTAN_LIBRARY_DIR): LIBS += -L$$QSSH_BOTAN_LIBRARY_DIR
    msvc: LIBS += -lbotan
    else: LIBS += -lbotan-2
}
msvc: QMAKE_CXXFLAGS += /wd4250
