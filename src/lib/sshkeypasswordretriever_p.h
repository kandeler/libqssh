#pragma once

#include <string>

namespace QSsh {
namespace Internal {

std::string get_passphrase();

} // namespace Internal
} // namespace QSsh
