include(../common.pri)

API_HEADERS = \
    $$PWD/sftpchannel.h \
    $$PWD/sftpdefs.h \
    $$PWD/sftpfilesystemmodel.h \
    $$PWD/ssh_global.h \
    $$PWD/sshconnection.h \
    $$PWD/sshconnectionmanager.h \
    $$PWD/sshdirecttcpiptunnel.h \
    $$PWD/ssherrors.h \
    $$PWD/sshforwardedtcpiptunnel.h \
    $$PWD/sshhostkeydatabase.h \
    $$PWD/sshkeycreationdialog.h \
    $$PWD/sshkeygenerator.h \
    $$PWD/sshpseudoterminal.h \
    $$PWD/sshremoteprocess.h \
    $$PWD/sshremoteprocessrunner.h \
    $$PWD/sshtcpipforwardserver.h

qssh_developer_build: API_HEADERS += $$PWD/sshx11displayinfo_p.h $$PWD/sshx11inforetriever_p.h
