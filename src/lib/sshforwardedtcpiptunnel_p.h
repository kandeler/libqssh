#pragma once

#include "sshforwardedtcpiptunnel.h"
#include "sshtcpiptunnel_p.h"

namespace QSsh {
namespace Internal {

class SshForwardedTcpIpTunnelPrivate : public SshTcpIpTunnelPrivate
{
    Q_OBJECT
    friend class QSsh::SshForwardedTcpIpTunnel;
public:
    SshForwardedTcpIpTunnelPrivate(quint32 channelId, SshSendFacility &sendFacility);
    void handleOpenSuccessInternal() override;
};

} // namespace Internal
} // namespace QSsh
