#pragma once

#include "ssh_global.h"

#include <QDialog>

namespace QSsh {
class SshKeyGenerator;

namespace Ui { class SshKeyCreationDialog; }

class QSSH_EXPORT SshKeyCreationDialog : public QDialog
{
    Q_OBJECT
public:
    SshKeyCreationDialog(QWidget *parent = 0);
    ~SshKeyCreationDialog();

    QString privateKeyFilePath() const;
    QString publicKeyFilePath() const;

private:
    void keyTypeChanged();
    void generateKeys();
    void handleBrowseButtonClicked();
    void setPrivateKeyFile(const QString &filePath);
    void saveKeys();
    bool userForbidsOverwriting();

private:
    SshKeyGenerator *m_keyGenerator;
    Ui::SshKeyCreationDialog *m_ui;
};

} // namespace QSsh
