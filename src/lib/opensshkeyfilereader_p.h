#pragma once

#include <QByteArray>
#include <QList>

#include <botan/bigint.h>

#include <memory>

namespace Botan {
class Private_Key;
class RandomNumberGenerator;
}

namespace QSsh {
namespace Internal {

class OpenSshKeyFileReader
{
public:
    OpenSshKeyFileReader(Botan::RandomNumberGenerator &rng) : m_rng(rng) {}

    bool parseKey(const QByteArray &privKeyFileContents);
    QByteArray keyType() const { return m_keyType; }
    std::unique_ptr<Botan::Private_Key> privateKey() const;
    QList<Botan::BigInt> allParameters() const { return m_parameters; }
    QList<Botan::BigInt> publicParameters() const;

private:
    void doParse(const QByteArray &payload);
    void parseKdfOptions(const QByteArray &kdfOptions);
    void decryptPrivateKeyList();
    void parsePrivateKeyList();
    [[noreturn]] void throwException(const QString &reason);

    Botan::RandomNumberGenerator &m_rng;
    QByteArray m_keyType;
    QList<Botan::BigInt> m_parameters;
    QByteArray m_cipherName;
    QByteArray m_kdf;
    QByteArray m_salt;
    quint32 m_rounds;
    QByteArray m_privateKeyList;
};

} // namespace Internal
} // namespace QSsh

