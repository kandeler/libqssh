#pragma once

#include "ssh_global.h"
#include <QIODevice>
#include <QSharedPointer>

namespace QSsh {

namespace Internal {
class SshChannelManager;
class SshForwardedTcpIpTunnelPrivate;
class SshSendFacility;
class SshTcpIpTunnelPrivate;
} // namespace Internal

class QSSH_EXPORT SshForwardedTcpIpTunnel : public QIODevice
{
    Q_OBJECT
    friend class Internal::SshChannelManager;
    friend class Internal::SshTcpIpTunnelPrivate;

public:
    typedef QSharedPointer<SshForwardedTcpIpTunnel> Ptr;
    ~SshForwardedTcpIpTunnel() override;

    // QIODevice stuff
    bool atEnd() const override;
    qint64 bytesAvailable() const override;
    bool canReadLine() const override;
    void close() override;
    bool isSequential() const override { return true; }

signals:
    void error(const QString &reason);

private:
    SshForwardedTcpIpTunnel(quint32 channelId, Internal::SshSendFacility &sendFacility);

    // QIODevice stuff
    qint64 readData(char *data, qint64 maxlen) override;
    qint64 writeData(const char *data, qint64 len) override;

    Internal::SshForwardedTcpIpTunnelPrivate * const d;
};

} // namespace QSsh
