#pragma once

#include "sshchannel_p.h"
#include <QIODevice>
#include <QByteArray>

namespace QSsh {
namespace Internal {

class SshTcpIpTunnelPrivate : public AbstractSshChannel
{
    Q_OBJECT

public:
    SshTcpIpTunnelPrivate(quint32 channelId, SshSendFacility &sendFacility);
    ~SshTcpIpTunnelPrivate();

    template<class SshTcpIpTunnel>
    void init(SshTcpIpTunnel *q)
    {
        connect(this, &SshTcpIpTunnelPrivate::closed,
                q, &SshTcpIpTunnel::close, Qt::QueuedConnection);
        connect(this, &SshTcpIpTunnelPrivate::readyRead,
                q, &SshTcpIpTunnel::readyRead, Qt::QueuedConnection);
        connect(this, &SshTcpIpTunnelPrivate::error, q, [q](const QString &reason) {
            q->setErrorString(reason);
            emit q->error(reason);
        }, Qt::QueuedConnection);
    }

    void handleChannelSuccess() override;
    void handleChannelFailure() override;

    qint64 readData(char *data, qint64 maxlen);
    qint64 writeData(const char *data, qint64 len);

signals:
    void readyRead();
    void error(const QString &reason);
    void closed();

protected:
    void handleOpenFailureInternal(const QString &reason) override;
    void handleChannelDataInternal(const QByteArray &data) override;
    void handleChannelExtendedDataInternal(quint32 type, const QByteArray &data) override;
    void handleExitStatus(const SshChannelExitStatus &exitStatus) override;
    void handleExitSignal(const SshChannelExitSignal &signal) override;
    void closeHook() override;

    QByteArray m_data;

private:
    void handleEof();
};

} // namespace Internal
} // namespace QSsh
