#pragma once

#include "ssh_global.h"

#include <QIODevice>
#include <QSharedPointer>

namespace QSsh {

namespace Internal {
class SshChannelManager;
class SshDirectTcpIpTunnelPrivate;
class SshSendFacility;
class SshTcpIpTunnelPrivate;
} // namespace Internal

class QSSH_EXPORT SshDirectTcpIpTunnel : public QIODevice
{
    Q_OBJECT

    friend class Internal::SshChannelManager;
    friend class Internal::SshTcpIpTunnelPrivate;

public:
    typedef QSharedPointer<SshDirectTcpIpTunnel> Ptr;

    ~SshDirectTcpIpTunnel();

    // QIODevice stuff
    bool atEnd() const;
    qint64 bytesAvailable() const;
    bool canReadLine() const;
    void close();
    bool isSequential() const { return true; }

    void initialize();

signals:
    void initialized();
    void error(const QString &reason);

private:
    SshDirectTcpIpTunnel(quint32 channelId, const QString &originatingHost,
            quint16 originatingPort, const QString &remoteHost, quint16 remotePort,
            Internal::SshSendFacility &sendFacility);

    // QIODevice stuff
    qint64 readData(char *data, qint64 maxlen);
    qint64 writeData(const char *data, qint64 len);

    Internal::SshDirectTcpIpTunnelPrivate * const d;
};

} // namespace QSsh
