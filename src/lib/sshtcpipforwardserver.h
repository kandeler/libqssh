#pragma once

#include "ssh_global.h"
#include "sshforwardedtcpiptunnel.h"
#include <QObject>

namespace QSsh {

namespace Internal {
class SshChannelManager;
class SshTcpIpForwardServerPrivate;
class SshSendFacility;
class SshConnectionPrivate;
} // namespace Internal

class QSSH_EXPORT SshTcpIpForwardServer : public QObject
{
    Q_OBJECT
    friend class Internal::SshChannelManager;
    friend class Internal::SshConnectionPrivate;

public:
    enum State {
        Inactive,
        Initializing,
        Listening,
        Closing
    };

    typedef QSharedPointer<SshTcpIpForwardServer> Ptr;
    ~SshTcpIpForwardServer();

    const QString &bindAddress() const;
    quint16 port() const;
    State state() const;
    void initialize();
    void close();

    SshForwardedTcpIpTunnel::Ptr nextPendingConnection();

signals:
    void error(const QString &reason);
    void newConnection();
    void stateChanged(State state);

private:
    SshTcpIpForwardServer(const QString &bindAddress, quint16 bindPort,
                          Internal::SshSendFacility &sendFacility);
    void setListening(quint16 port);
    void setClosed();
    void setNewConnection(const SshForwardedTcpIpTunnel::Ptr &connection);

    Internal::SshTcpIpForwardServerPrivate * const d;
};

} // namespace QSsh
