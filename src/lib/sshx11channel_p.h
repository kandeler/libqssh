#pragma once

#include "sshchannel_p.h"

#include "sshx11displayinfo_p.h"

#include <QByteArray>

namespace QSsh {
namespace Internal {
class SshChannelManager;
class SshSendFacility;
class X11Socket;

class SshX11Channel : public AbstractSshChannel
{
    Q_OBJECT

    friend class Internal::SshChannelManager;

signals:
    void error(const QString &message);

private:
    SshX11Channel(const X11DisplayInfo &displayInfo, quint32 channelId,
                  SshSendFacility &sendFacility);

    void handleChannelSuccess() override;
    void handleChannelFailure() override;

    void handleOpenSuccessInternal() override;
    void handleOpenFailureInternal(const QString &reason) override;
    void handleChannelDataInternal(const QByteArray &data) override;
    void handleChannelExtendedDataInternal(quint32 type, const QByteArray &data) override;
    void handleExitStatus(const SshChannelExitStatus &exitStatus) override;
    void handleExitSignal(const SshChannelExitSignal &signal) override;
    void closeHook() override;

    void handleRemoteData(const QByteArray &data);

    X11Socket * const m_x11Socket;
    const X11DisplayInfo m_displayInfo;
    QByteArray m_queuedRemoteData;
    bool m_haveReplacedRandomCookie = false;
};

} // namespace Internal
} // namespace QSsh
