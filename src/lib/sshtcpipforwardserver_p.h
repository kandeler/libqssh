#pragma once

#include "sshtcpipforwardserver.h"
#include <QList>
#include <QTimer>

namespace QSsh {
namespace Internal {

class SshTcpIpForwardServerPrivate
{
public:
    static const int ReplyTimeout = 10000; // milli seconds

    SshTcpIpForwardServerPrivate(const QString &bindAddress, quint16 bindPort,
                                 SshSendFacility &sendFacility);

    SshSendFacility &m_sendFacility;
    QTimer m_timeoutTimer;

    const QString m_bindAddress;
    quint16 m_bindPort;
    SshTcpIpForwardServer::State m_state;

    QList<SshForwardedTcpIpTunnel::Ptr> m_pendingConnections;
};

} // namespace Internal
} // namespace QSsh
