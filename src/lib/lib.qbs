import qbs
import qbs.FileInfo
import qbs.Probes

DynamicLibrary {
    name: "qssh"
    type: base.concat("hpp")

    Depends { name: "Qt"; submodules: ["widgets", "network" ] }

    cpp.cxxLanguageVersion: "c++17"
    cpp.defines: project.commonDefines.concat("QSSH_LIBRARY")

    Probes.PkgConfigProbe {
        id: botanProbe
        condition: !project.botanIncludeDir
        name: "botan-2"
    }

    cpp.includePaths: {
        if (project.botanIncludeDir)
            return project.botanIncludeDir;
        if (botanProbe.found)
            return botanProbe.includePaths;
        if (qbs.targetOS.contains("unix"))
            return "/usr/include/botan-2";
    }
    cpp.libraryPaths: project.botanLibraryDir
    cpp.dynamicLibraries: {
        if (botanProbe.found)
            return botanProbe.libraries;
        var result = [qbs.toolchain.contains("msvc") ? "botan" : "botan-2"];
        if (qbs.targetOS.contains("windows"))
            result.push("advapi32", "user32", "ws2_32")
        else if (qbs.targetOS.contains("linux"))
            result.push("rt", "dl");
        else if (qbs.targetOS.contains("macos"))
            result.push("dl");
        else if (qbs.targetOS.contains("unix"))
            result.push("rt");
        return result;
    }

    Properties {
        condition: qbs.toolchain.contains("msvc")
        cpp.cxxFlags: base.concat("/wd4250");
    }

    Group {
        name: "API headers"
        qbs.install: true
        qbs.installDir: project.headersInstallBaseDir + "/qssh"
        files: [
            "sftpchannel.h",
            "sftpdefs.h",
            "sftpfilesystemmodel.h",
            "sshconnection.h",
            "sshconnectionmanager.h",
            "sshdirecttcpiptunnel.h",
            "ssherrors.h",
            "sshforwardedtcpiptunnel.h",
            "ssh_global.h",
            "sshhostkeydatabase.h",
            "sshkeycreationdialog.h",
            "sshkeygenerator.h",
            "sshpseudoterminal.h",
            "sshremoteprocess.h",
            "sshremoteprocessrunner.h",
            "sshtcpipforwardserver.h",
        ]
    }

    Group {
        name: "Internal headers needed by autotest"
        qbs.install: project.developerBuild
        qbs.installDir: project.headersInstallBaseDir + "/qssh"
        files: [
            "sshx11displayinfo_p.h",
            "sshx11inforetriever_p.h",
        ]
    }

    files: [
        "opensshkeyfilereader.cpp",
        "opensshkeyfilereader_p.h",
        "sftpchannel_p.h",
        "sftpchannel.cpp",
        "sftpdefs.cpp",
        "sftpfilesystemmodel.cpp",
        "sftpincomingpacket.cpp",
        "sftpincomingpacket_p.h",
        "sftpoperation.cpp",
        "sftpoperation_p.h",
        "sftpoutgoingpacket.cpp",
        "sftpoutgoingpacket_p.h",
        "sftppacket.cpp",
        "sftppacket_p.h",
        "ssh.qrc",
        "sshagent.cpp",
        "sshagent_p.h",
        "sshbotanconversions_p.h",
        "sshcapabilities_p.h",
        "sshcapabilities.cpp",
        "sshchannel.cpp",
        "sshchannel_p.h",
        "sshchannelmanager.cpp",
        "sshchannelmanager_p.h",
        "sshconnection_p.h",
        "sshconnection.cpp",
        "sshconnectionmanager.cpp",
        "sshcryptofacility.cpp",
        "sshcryptofacility_p.h",
        "sshdirecttcpiptunnel_p.h",
        "sshdirecttcpiptunnel.cpp",
        "sshexception_p.h",
        "sshforwardedtcpiptunnel.cpp",
        "sshforwardedtcpiptunnel_p.h",
        "sshhostkeydatabase.cpp",
        "sshincomingpacket_p.h",
        "sshincomingpacket.cpp",
        "sshkeycreationdialog.cpp",
        "sshkeycreationdialog.ui",
        "sshkeyexchange.cpp",
        "sshkeyexchange_p.h",
        "sshkeygenerator.cpp",
        "sshkeypasswordretriever.cpp",
        "sshkeypasswordretriever_p.h",
        "sshlogging.cpp",
        "sshlogging_p.h",
        "sshoutgoingpacket.cpp",
        "sshoutgoingpacket_p.h",
        "sshpacket.cpp",
        "sshpacket_p.h",
        "sshpacketparser.cpp",
        "sshpacketparser_p.h",
        "sshremoteprocess.cpp",
        "sshremoteprocess_p.h",
        "sshremoteprocessrunner.cpp",
        "sshsendfacility.cpp",
        "sshsendfacility_p.h",
        "sshtcpipforwardserver.cpp",
        "sshtcpipforwardserver_p.h",
        "sshtcpiptunnel.cpp",
        "sshtcpiptunnel_p.h",
        "sshx11channel.cpp",
        "sshx11channel_p.h",
        "sshx11inforetriever.cpp",
    ]

    Group {
        fileTagsFilter: ["dynamiclibrary", "dynamiclibrary_symlinks"]
        qbs.install: true
        qbs.installDir: project.libInstallDir
    }

    Export {
        Depends { name: "cpp" }
        Depends { name: "Qt"; submodules: ["widgets", "network"] }
        cpp.includePaths: FileInfo.joinPaths(qbs.installRoot, qbs.installPrefix,
                                             project.headersInstallBaseDir)
    }
}
