#pragma once

#include "ssh_global.h"

#include <QObject>
#include <QString>

QT_BEGIN_NAMESPACE
class QByteArray;
class QProcess;
class QTemporaryFile;
QT_END_NAMESPACE

namespace QSsh {
namespace Internal {
class X11DisplayInfo;

class QSSH_AUTOTEST_EXPORT SshX11InfoRetriever : public QObject
{
    Q_OBJECT
public:
    SshX11InfoRetriever(const QString &displayName, QObject *parent = nullptr);
    void start();

signals:
    void failure(const QString &message);
    void success(const X11DisplayInfo &displayInfo);

private:
    void emitFailure(const QString &reason);

    const QString m_displayName;
    QProcess * const m_xauthProc;
    QTemporaryFile * const m_xauthFile;

    enum class State { Inactive, RunningGenerate, RunningList } m_state = State::Inactive;
};

} // namespace Internal
} // namespace QSsh
