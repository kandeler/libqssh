include(lib.pri)

TEMPLATE = aux

copy_headers.input = API_HEADERS
copy_headers.output = $$QSSH_INCLUDE_DIR/qssh/${QMAKE_FILE_IN_BASE}.h
copy_headers.commands = $$QMAKE_COPY ${QMAKE_FILE_IN} ${QMAKE_FILE_OUT}
copy_headers.name = COPY ${QMAKE_FILE_IN}
# copy_headers.dependency_type = TYPE_H
copy_headers.CONFIG += no_link target_predeps
QMAKE_EXTRA_COMPILERS += copy_headers

