#include "sftppacket_p.h"

#include "sshpacketparser_p.h"

namespace QSsh {
namespace Internal {

const quint32 AbstractSftpPacket::MaxDataSize = 32000;
const quint32 AbstractSftpPacket::MaxPacketSize = 34000;
const int AbstractSftpPacket::TypeOffset = 4;
const int AbstractSftpPacket::RequestIdOffset = TypeOffset + 1;
const int AbstractSftpPacket::PayloadOffset = RequestIdOffset + 4;

AbstractSftpPacket::AbstractSftpPacket()
{
}

quint32 AbstractSftpPacket::requestId() const
{
    return SshPacketParser::asUint32(m_data, RequestIdOffset);
}

} // namespace Internal
} // namespace QSsh
