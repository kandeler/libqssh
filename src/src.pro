include(common.pri)

TEMPLATE = subdirs
actual_lib.file = lib/lib.pro
static_lib.file = lib/lib-static.pro
SUBDIRS = actual_lib static_lib examples
qssh_developer_build: SUBDIRS += test

test.depends = actual_lib static_lib
examples.depends = actual_lib static_lib
