include(../common.pri)

TEMPLATE = app
TARGET = tst_ssh
QT = core network testlib
CONFIG += testcase console
INCLUDEPATH += $$QSSH_INCLUDE_DIR
LIBS += -L$$QSSH_LIB_DIR -lqssh
DESTDIR = $$QSSH_BIN_DIR

SOURCES += tst_ssh.cpp
