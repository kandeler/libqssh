import qbs

QSshTestApp {
    name: "SSH autotest"
    type: base.concat("autotest")
    Depends { name: "Qt.testlib" }
    files: "tst_ssh.cpp"
}
