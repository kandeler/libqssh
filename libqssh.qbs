import qbs

Project {
    references: "src"
    qbsSearchPaths: "qbs"

    property bool developerBuild: false
    property string libInstallDir: "lib"
    property string headersInstallBaseDir: "include"
    property stringList commonDefines: developerBuild ? ["WITH_TESTS"] : []
    property string botanIncludeDir
    property string botanLibraryDir

    Product {
        name: "info"
        files: [
            "COPYRIGHT.txt",
            "LICENSE.txt",
            "README.md",
        ]
    }

    Product {
        name: "qmake project files"
        files: [".qmake.conf", "*.pro", "**/*.pro"]
    }

    AutotestRunner {}
}
